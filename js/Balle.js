﻿let balleState = {
    normal: "images/eau1.png",
	warning: "images/eau2.png",
    danger: "images/eau3.png",
	plus: "images/etincelle.png"
};

class Balle extends ObjetGraphique {
	constructor(x, y, rayon, vx, vy, state) {
		super(x, y, state, vx, vy);
		this.rayon = rayon;
	}
  
	draw(ctx) {
		super.draw(ctx);
		ctx.save(); 
		var image = new Image();
		image.src = this.couleur; 
		ctx.drawImage(image, this.x-this.rayon, this.y-this.rayon, this.rayon*2, this.rayon*2);
		ctx.restore();	
	}
	changerCouleur(){
		if(this.couleur == balleState.normal){
			this.couleur = balleState.warning;
		}
		else if(this.couleur == balleState.warning){
			this.couleur = balleState.danger;
		}
	}
}
