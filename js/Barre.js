﻿class Barre extends Rectangle {

	constructor(widthParent, x, y, longueur, largeur) {
		super(x, y, longueur, largeur, "black");
		this.widthParent = widthParent;
		this.updateVariable();
	}
	
	updateVariable(){
		this.xbarreAvant = 0;
		this.lgbarreAvant = this.x;
		this.xbarreApres = this.x+this.longueur;
		this.lgbarreApres = this.widthParent-this.x-this.longueur;
	}
	
	
	draw(ctx){
		super.draw(ctx);
		ctx.save(); 
		if (this.longueur < canvas.width){
			ctx.fillStyle = "white";
			ctx.fillRect(this.xbarreAvant, this.y, this.x, this.largeur);
			ctx.fillRect(this.xbarreApres, this.y, this.widthParent-this.x-this.longueur, this.largeur);
		}
		ctx.restore();
		
	}
	
	setX (newX){
		if (newX+this.longueur <= canvas.width && newX >= 0){
			this.x = newX;
			this.updateVariable();
		}
	}
	
	setLongueur(){
		this.longueur = this.longueur-10;
		this.updateVariable();
	}
}
