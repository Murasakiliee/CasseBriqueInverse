﻿
function MoteurJeu() {
	let canvas, ctx, width, height;
	let barre, score, dScore = 50;
	let tableauxBriques = [];
	let tableauxDesBalles = [];
	let tableauxDesEtincelles = [];
	let combo = "";
	let nbcombo = 0;
	let niveau = 1, dNiveau = 400, dRayon = 15;
	let sonGOPlayed = false;
	
	let gameStates = {
        gameStart: 0,
		gameRunning: 1,
        gameOver: 2,
		gamePaused : 3
    };
	
	let currentGameState ;
	let displayingCredits, displayingHelp, displayingHighScores, displayingMenuPause;
	
	function move(evt){
		let rect = canvas.getBoundingClientRect();
		let mx = evt.clientX - rect.left;
		barre.setX(mx);
	}
	
	function setPause(display){
		if (currentGameState == gameStates.gameRunning || display != null){
			currentGameState = gameStates.gamePaused;
		}
		else if (currentGameState == gameStates.gameOver){
			init();
		}
		else{
			currentGameState = gameStates.gameRunning;
		}
	}
	
	function toucheEnfoncee(evt) {
		switch(evt.key) {
			case ' ' :
				setPause();
				displayingHelp = false;
				displayingMenuPause = true;
				displayingHighScores = false;
				displayingCredits = false;
				break;
			case 'h' :
				setPause('h');
				displayingHelp = true;
				displayingHighScores = false;
				displayingCredits = false;
				displayingMenuPause = false;
				break;
			case 'c' :
				setPause('c');
				displayingCredits = true;
				displayingHelp = false;
				displayingHighScores = false;
				displayingMenuPause = false;
				break;
			case 's' :
				setPause('s');
				displayingHighScores = true;
				displayingCredits = false;
				displayingHelp = false;
				displayingMenuPause = false;
				break;
		}
	}
	
	function addEcouteurs() {
		canvas.addEventListener('mousemove', move);
		window.addEventListener('keydown', toucheEnfoncee);		
	}  
	
	function testeCollisionBalleAvecBrique(b) {
		for(let i = 0; i < tableauxBriques.length; i++) {
			let collision = circRectsOverlap(tableauxBriques[i].x, tableauxBriques[i].y, tableauxBriques[i].longueur, tableauxBriques[i].largeur, b.x, b.y, b.rayon)
			if (collision == true){
				tableauxBriques.splice(i,1);
				if(b.couleur == balleState.danger){faireDisparaitre(b);addBall();}
				b.vy = -b.vy;
				break;
			}
		}
		
	}
	function testeCollisionBalleAvecMurs(b) {
		if(((b.x + b.rayon) > canvas.width) || (b.x  < 0)) {
			b.vx = -b.vx;
		}
		if (b.y < 0) {
			b.vy = -b.vy;
		}
		if((b.y + 2*b.rayon) > canvas.height){
			nbcombo++;
			combo = nbcombo+" Combo! ";
			score = score+dScore;
			faireDisparaitre(b);
			playCasse();
		}
	}
	function testeCollisionBalleAvecRaquette(b) {
		let collisionBarre1 = circRectsOverlap(barre.xbarreAvant, barre.y, barre.lgbarreAvant, barre.largeur, b.x, b.y, b.rayon)
		let collisionBarre2 = circRectsOverlap(barre.xbarreApres, barre.y, barre.lgbarreApres, barre.largeur, b.x, b.y, b.rayon)
		if (collisionBarre1 == true || collisionBarre2 == true){
			b.vy = -b.vy;
			nbcombo=0;
			combo = "";
			b.changerCouleur();
		}
	}
	
	function testeCollisionBonusAvecRaquette(b) {
		let collisionBarre1 = circRectsOverlap(barre.xbarreAvant, barre.y, barre.lgbarreAvant, barre.largeur, b.x, b.y, b.rayon)
		let collisionBarre2 = circRectsOverlap(barre.xbarreApres, barre.y, barre.lgbarreApres, barre.largeur, b.x, b.y, b.rayon)
		if (collisionBarre1 == true || collisionBarre2 == true){
			faireDisparaitreBonus(b);
		}
	}
	
	function testeCollisionBonus(b) {
		if((b.y + 2*b.rayon) > canvas.height){
			let bonusScore = dScore*2;
			score = score+bonusScore;
			combo = "Bonus";
			faireDisparaitreBonus(b);
		}
	}
	
	function dessinerBriques() {
		if(tableauxBriques.length == 0){
			gameOver();
		}
		tableauxBriques.forEach(function(b, index, tab) {
			b.draw(ctx);
		});
	}
	function creerBriques(){
		let couleur = ["darkgrey", "grey", "lightgrey", "lightblue"];
		let nbligne = 4;
		let espace = 4;
		let ncol = 8;
		let y = 40;
		let largeur = 50;
		
		for(let i = 0; i < nbligne; i++) {
			let x = 0;
			let dimension = 0;
			while(dimension < width){
				let longueur = 20+Math.random()*canvas.width/ncol-espace;
				dimension += longueur;
				let brique = new Nuage(dimension-longueur, y, longueur,largeur, couleur[i] , nbligne-i);
				tableauxBriques.push(brique);
				x = x+canvas.width/ncol;
			}
			y = y+largeur+espace;
		}
	}

		
	function dessinerEtDeplacerLesBalles() {  
		if (tableauxDesEtincelles.length > 0){
			tableauxDesEtincelles.forEach(function(bonus, index, tab) {
				bonus.move();
				bonus.draw(ctx);
				testeCollisionBonus(bonus);
				testeCollisionBalleAvecBrique(bonus);
				testeCollisionBonusAvecRaquette(bonus);
		  });
		 }
		tableauxDesBalles.forEach(function(b, index, tab) {
			b.move();
			b.draw(ctx);
			testeCollisionBalleAvecMurs(b);
			testeCollisionBalleAvecBrique(b);
			testeCollisionBalleAvecRaquette(b);
		});
		
	  }
		
	
	function addBall(){	
		if (currentGameState == gameStates.gameRunning){
			let x = Math.random() * (canvas.width- 2*5); 
			let y = (6*35)+Math.random() *(canvas.height-300);
			let vy = 1+Math.random()*4;
			let vx = Math.random();
			if (vx < 0.5) vx = -1;
			else vx = 1;
			let b = new Balle(x, y, dRayon, vx,vy, balleState.normal);
			tableauxDesBalles.push(b);
		}
	}

	function gameOver(){
		if(!sonGOPlayed){
			playGameover();
			sonGOPlayed = true;
		}
		currentGameState = gameStates.gameOver;
		displayMenuGameOver(ctx, score);
		tableauxDesBalles = [];
		tableauxBriques = [];
	}
	
	function faireDisparaitre(b){
	  for(let i = 0; i < tableauxDesBalles.length; i++) {
			if(b.x == tableauxDesBalles[i].x){ 
			tableauxDesBalles.splice(i,1);  }
		}
	}
	
	function faireDisparaitreBonus(b){
	  for(let i = 0; i < tableauxDesEtincelles.length; i++) {
			if(b.x == tableauxDesEtincelles[i].x){ 
			tableauxDesEtincelles.splice(i,1);  }
		}
	}
	
	function addEtincelle(niveau){
		if (currentGameState == gameStates.gameRunning){
			for (let i = 0; i < niveau; i++){
				let vy = 1+Math.random()*4;
				let vx = Math.random();
				if (vx < 0.5) vx = -1;
				else vx = 1;
				let b = new Balle(canvas.width/2, canvas.height-300, dRayon+2, vx,vy, balleState.plus);
				tableauxDesEtincelles.push(b);
			}
		}
	}
	
	function controleJeu(){
		
		if(tableauxBriques.length == 0) gameOver();
		if(currentGameState == gameStates.gameRunning){
			score ++;
		}
		if(score !=0 && score%dNiveau==0){
			niveau++;
			barre.setLongueur();
			dScore += 50;
			if(dRayon > 0) dRayon --;
			dNiveau += 2 * dNiveau;
			playNewLevel();
			addEtincelle(niveau);
		}
	}
	
	function mainLoop(time) {
		ctx.clearRect(0, 0, width, height);
		barre.draw(ctx); 
		dessinerBriques();
		
		controleJeu();
		
		switch (currentGameState) {
            case gameStates.gameRunning:
				dessinerEtDeplacerLesBalles();
				displayScore(ctx, score, niveau, combo, height);
				break;
			case gameStates.gameStart:
				drawPauseScreen(ctx, width, height);
				displayMenu(ctx);
                break;
			case gameStates.gamePaused:
				drawPauseScreen(ctx, width, height);
				if (displayingCredits == true) displayCredit(ctx);
				if (displayingMenuPause == true) displayMenuPause(ctx);
				if (displayingHelp == true) displayHelp(ctx);
				if (displayingHighScores == true) displayHighScores(ctx,score,0);
				break;
        }
		requestAnimationFrame(mainLoop);
	}	
	
	function start(){
		canvas = document.querySelector("#myCanvas");
  		width = canvas.width;
  		height = canvas.height; 
  		ctx = canvas.getContext('2d');
		setInterval(addBall,1500);
		init();
		addEcouteurs(this);
  		requestAnimationFrame(mainLoop);
	}
	
	function init() {
		tableauxDesBalles = [];
		tableauxBriques = [];
		creerBriques();
		currentGameState = gameStates.gameStart;
		score = 0;
		barre = new Barre(canvas.width, canvas.width/2, canvas.height-70, 120, 10);
		barre.draw(ctx);
		sonGOPlayed = false;
	}
	
	// API du moteur de jeu
	return {
		start:start
	}
	
}
