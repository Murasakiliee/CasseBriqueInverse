﻿class Nuage extends ObjetGraphique {
  constructor(x, y, longueur, largeur, couleur, niveau) {
    // appel du constructeur hérité
    super(x, y, couleur, 0, 0);
    this.longueur = longueur;
	this.largeur = largeur;
	this.niveau = niveau;
  }
  
  draw(ctx) {
    super.draw(ctx);
    ctx.save(); 
   
	ctx.fillStyle = this.couleur;
    ctx.fillRect(this.x, this.y, this.longueur, this.largeur);
   
	var image = new Image();
	image.src = "images/nuage"+this.niveau+".png"; 
	ctx.drawImage(image, this.x, this.y, this.longueur, this.largeur);
	
    ctx.restore();
        
  }
}
