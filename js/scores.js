﻿function setHighscores(score){
	if(typeof localStorage!='undefined') {
		if(score > localStorage.getItem('meilleurs1')){
			localStorage.setItem("meilleurs2",localStorage.getItem('meilleurs1'));
			localStorage.setItem("meilleurs1",score);
		}
		else if(score >= localStorage.getItem('meilleurs2') && score < localStorage.getItem('meilleurs1')){
				localStorage.setItem("meilleurs2",score);
		}
	}
	else{ 
		localStorage.setItem("meilleurs1",0);
		localStorage.setItem("meilleurs2",0);
	}
}