﻿function displayScore(ctx, score, niveau, combo, height){
	ctx.save();
	let fontSize = 20;
	ctx.font = fontSize + 'px Courier bold';
	ctx.fillStyle = 'white';
	ctx.fillText("SCORE : "+score,10,30);
	ctx.fillText("NIVEAU : "+niveau,210,30);
	fontSize = 35;
	ctx.font = fontSize + 'px Courier BOLD';
	ctx.fillStyle = 'green';
	ctx.fillText(combo,50,height-200);	
	ctx.restore();
}
function displayCredit(ctx){
    ctx.save();
	ctx.font = '40px Courier bold';
	ctx.fillStyle = 'white';
	ctx.fillText("CREDITS",120,100);
	ctx.font = '20px Courier BOLD';
	ctx.fillStyle = 'white';
	ctx.fillText("Etudiantes MBDS Madagascar",80,160);
	ctx.fillText("2016 - 2017",150,200);
	ctx.fillText("LIANTSOA Murielle",110,240);
	ctx.fillText("RAMAMONJISOA Hajatiana",80,280);
    ctx.restore();
}

function drawPauseScreen(ctx, width, height) {
    ctx.save();
    ctx.fillStyle = 'rgba(70, 80, 80, 0.7)';
    ctx.fillRect(0, 0, width, height);
    ctx.restore();
}

function displayMenuPause(ctx){
	ctx.save();
	ctx.font = '40px Courier bold';
	ctx.fillStyle = 'white';
	ctx.fillText("MENU",140,100);
	ctx.font = '20px Courier BOLD';
	ctx.fillStyle = 'white';
	ctx.fillText("Continuer (Espace)",120,160);
	ctx.fillText("Aide (H)",160,200);
	ctx.fillText("Meilleures scores (S)",110,240);
	ctx.fillText("Credits (C)",150,280);
    ctx.restore();
}

function displayMenu(ctx){
	ctx.save();
	ctx.font = '40px Courier bold';
	ctx.fillStyle = 'white';
	ctx.fillText("MENU",140,100);
	ctx.font = '20px Courier BOLD';
	ctx.fillStyle = 'white';
	ctx.fillText("Nouvelle partie (Espace)",100,160);
	ctx.fillText("Aide (H)",160,200);
	ctx.fillText("Meilleures scores (S)",110,240);
	ctx.fillText("Credits (C)",150,280);
    ctx.restore();
}

function displayMenuGameOver(ctx, score){
	ctx.save();
	setHighscores(score);
	ctx.font = '40px Courier bold';
	ctx.fillStyle = 'white';
	ctx.fillText("SCORE : "+score,90,100);
	ctx.font = '20px Courier BOLD';
	ctx.fillStyle = 'white';
	ctx.fillText("Nouvelle partie (Espace)",100,400);
	ctx.restore();
	displayHighScores(ctx,score, 100);
} 

function displayHelp(ctx){
	ctx.save();
	ctx.font = '40px Courier bold';
	ctx.fillStyle = 'white';
	ctx.fillText("AIDE",140,100);
	ctx.font = '15px Courier BOLD';
	ctx.fillStyle = 'white';
	ctx.fillText("Le but du jeu est d'éviter de" ,100,160);
	ctx.fillText("toucher les nuages et envoyer les gouttes d'eau entre",30,190);
	ctx.fillText("les raquettes",150,220);
	ctx.fillText("Déplacer votre souris pour déplacer la raquette",50,250);
    ctx.restore();
}

function displayHighScores(ctx,score, heightPlus){
	ctx.save();
	ctx.translate(0, heightPlus);
	ctx.font = '38px Courier bold';
	ctx.fillStyle = 'white';
	ctx.fillText("MEILLEURES SCORES",10,100);
	ctx.font = '20px Courier BOLD';
	ctx.fillStyle = 'white';
	setHighscores(score);
	ctx.fillText("1 : "+localStorage.getItem('meilleurs1'),50,160);
	ctx.fillText("2 : "+localStorage.getItem('meilleurs2'),50,190);
    ctx.restore();
}

function drawPauseScreen(ctx, width, height) {
    ctx.save();
    ctx.fillStyle = 'rgba(80, 80, 80, 0.7)';
    ctx.fillRect(0, 0, width, height);
    ctx.restore();
}